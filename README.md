# Rappatriement des données de prédiction en local
Projet permettant de télécharger les données de prédictions depuis un bucket AWS S3.

## Utilisation

Pour le bon dérouler de ce script il est nécessaire de pouvoir utiliser les commandes AWS CLI.

### Parametration

Les variables à modifié sont dans le fichier **env.config**

La variable *aws_s3* correspond au nom du bucket contenant les prédictions
La variable *fs_root* correspond au dossier de dépose des fichiers de prédictions, si il n'existe pas, il sera créer.

### Lancement 

Utilisez la commande
```
./get_aws.sh
```
